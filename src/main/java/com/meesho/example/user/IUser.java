package com.meesho.example.user;

public interface IUser {
	public String getName();
	public String getId();
}
