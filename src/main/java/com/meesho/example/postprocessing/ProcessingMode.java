package com.meesho.example.postprocessing;

public enum ProcessingMode {
	UNKNOWN, SMS, EMAIL;
}
