package com.meesho.example.postprocessing;

import com.meesho.example.order.Order;

public interface IPostProcessing {
	public boolean syncProcess(Order order) throws Exception;
	public void asyncProcess(Order order);
}
