package com.meesho.example.postprocessing.consumer;

import com.meesho.example.postprocessing.common.IEvent;

public interface IEventConsumer {
	public void consume(IEvent event);
}
