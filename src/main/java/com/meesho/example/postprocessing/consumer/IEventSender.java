package com.meesho.example.postprocessing.consumer;

import com.meesho.example.postprocessing.common.IEvent;

public interface IEventSender {
	public boolean send(IEvent event) throws Exception;
}
