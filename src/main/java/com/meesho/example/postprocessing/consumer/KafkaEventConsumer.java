package com.meesho.example.postprocessing.consumer;

import com.meesho.example.postprocessing.common.IEvent;

public abstract class KafkaEventConsumer implements IEventConsumer, IEventSender{
	// start consumer group via topic etc
	// and consume is called for every record
	
	
	public abstract boolean isValid(IEvent event);
	
	public void consume(IEvent event) {
		// TODO Auto-generated method stub
		// retry logic etc
		// validation checks
		try {
			send(event);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


}
