package com.meesho.example.postprocessing.common;

import com.meesho.example.order.Order;

public interface IEvent {
	Order getOrder();
	String toString();
}
