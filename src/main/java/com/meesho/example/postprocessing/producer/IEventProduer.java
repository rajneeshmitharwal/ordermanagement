package com.meesho.example.postprocessing.producer;

import com.meesho.example.postprocessing.common.IEvent;

public interface IEventProduer {
	public void produce(IEvent event);
}
