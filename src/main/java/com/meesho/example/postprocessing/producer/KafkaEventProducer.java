package com.meesho.example.postprocessing.producer;

import com.meesho.example.postprocessing.common.IEvent;

public abstract class KafkaEventProducer implements IEventProduer{

	static class KafkaProducer{
		void send(String message) {

		}
	}
	KafkaProducer kafkaProducer;

	public void produce(IEvent event) {

		event = modification(event);
		//with retry
		kafkaProducer.send(event.toString());
		// TODO Auto-generated method stub
	}

	public abstract IEvent modification(IEvent event);
	public int getRetryCount() {
		return 3; // free to override
	}
}
